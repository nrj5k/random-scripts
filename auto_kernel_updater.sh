#! /usr/bin/env bash

set -e
set -u
#set -x

CWD=$(pwd)
KERNEL_DL_LOC="/usr/src/"
# KERNEL_DL_LOC="/root"
BASEVERSION=5.10
N_MINORVERSION=$(curl https://www.kernel.org/ | grep $BASEVERSION | grep sign | sed -n 's/.*\=.*".*linux-\(.*\).tar\.sign\".*/\1/p' | sort -n | head -n 1| awk -F"." '{ print $3 }')
CT_MINORVERSION=$(uname -r | awk -F "." '{print $3}')
C_MINORVERSION=$(ls -1 $KERNEL_DL_LOC | grep linux-${BASEVERSION} | sort --version-sort --field-separator=- --reverse | head -n 1 | awk -F"." '{ print $3 }')

function delete_kernel_txz(){
        cd $KERNEL_DL_LOC
        rm -f linux-$1.tar.xz
        cd -
}

function delete_kernel(){
        MINORVERSION=$1
        cd $KERNEL_DL_LOC
        rm -rf linux-${MINORVERSION}
        cd -
}

function download_kernel(){
        VERSION=$1
        URL="https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$VERSION.tar.xz"
        cd $KERNEL_DL_LOC
        wget "$URL"
        cd -

}
function compile_kernel_and_install(){

        VERSION=$1
        cd $KERNEL_DL_LOC
        tar -xvpf "linux-$VERSION.tar.xz"

        cd linux-$VERSION
        zcat /proc/config.gz > .config
        make olddefconfig

        make -j16 bzImage
        make -j16 modules && make modules_install

        cp arch/x86/boot/bzImage /boot/vmlinuz-generic-$VERSION
        cp System.map /boot/System.map-generic-$VERSION
        cp .config /boot/config-generic-$VERSION

        cd /boot
        rm System.map
        rm config
        ln -s System.map-generic-$VERSION System.map
        ln -s config-generic-$VERSION config

        cd $HOME

        sh /usr/share/mkinitrd/mkinitrd_command_generator.sh --run /boot/vmlinuz-generic-$VERSION -a '-P /boot/intel-ucode.cpio' -m bcache | sh
        cd /boot/
        cp -v initrd.gz efi/EFI/Slackware/initrd.gz
        cp -vL vmlinuz-generic-$VERSION efi/EFI/Slackware/vmlinuz
        cd ${CWD}

}

if [[ $CT_MINORVERSION == $N_MINORVERSION ]]; then
        echo "No Kernel change, exiting"
        exit 0
elif [[ $CT_MINORVERSION -lt $C_MINORVERSION && $N_MINORVERSION == $C_MINORVERSION ]]; then
        echo "Kernel compiled, reboot pending"
        exit 0
else
        MINORVERSION=$N_MINORVERSION
        VERSION=${BASEVERSION}.${MINORVERSION}
        download_kernel $VERSION
        compile_kernel_and_install $VERSION
        delete_kernel_txz ${BASEVERSION}.${MINORVERSION}
        ## not tested yet, deleting is based on the version
        # delete_kernel ${BASEVERSION}.$CT_MINORVERSION
        # delete_kerel ${BASEVERSION}.$C_MINORVERSION
fi
