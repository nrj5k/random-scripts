#! /usr/bin/env bash

MPIVERSION="3.4.1"
ORANGEFSVERSION="2.9.8"
HDF5VERSION="1_12_0"
IORVERSION="3.3.0"

mkdir -p $HOME/sources
mkdir -p $HOME/root

cd sources/

wget http://www.mpich.org/static/downloads/$MPIVERSION/mpich-$MPIVERSION.tar.gz
tar zxvf mpich-$MPIVERSION.tar.gz
cd mpich-$MPIVERSION/
./configure --enable-romio --enable-shared --with-pvfs2=/home/$USER/root/ --prefix=/home/$USER/root/ --with-file-system=pvfs2
make
make install
cd -

wget http://download.orangefs.org/current/source/orangefs-$ORANGEFSVERSION.tar.gz
tar zxvf orangefs-$ORANGEFSVERSION.tar.gz 
cd orangefs-v.$ORANGEFSVERSION/
./configure --prefix /home/$USER/root/ --enable-shared --with-kernel=/lib/modules/3.10.0-862.el7.x86_64/build/
make install
cd -

wget https://github.com/HDFGroup/hdf5/archive/hdf5-$HDF5VERSION.tar.gz
tar zxvf hdf5-$HDF5VERSION.tar.gz
cd hdf5-hdf5-$HDF5VERSION/
CC=mpicc ./configure --enable-parallel --enable-build-mode=production  --prefix=/home/$USER/root
make install
cd -

wget https://github.com/hpc/ior/releases/download/$IORVERSION/ior-$IORVERSION.tar.gz
tar zxvf ior-$IORVERSION.tar.gz
cd ior-$IORVERSION/
./configure --with-hdf5 --prefix /home/$USER/root
make install
cd -
